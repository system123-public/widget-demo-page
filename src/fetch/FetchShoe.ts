import { IShoe } from '@/interface/interface';
import testdbPromise from '@/testdb-promise';
import { ref } from 'vue';

const get = () => {
  const shoe = ref<IShoe[]>([]);
  const recommendations = ref<IShoe[]>([]);
  const isFetching = ref<boolean>(false);

  const load = async (id: string) => {
    try {
      isFetching.value = true;
      // const response = await fetch(
      //   'https://freeshoesapi-production.up.railway.app/api/v1/shoes/' + id + '?recommendation=true'
      // );
      const testdb = await testdbPromise;

      const data = testdb.data.filter((p) => p.id === parseInt(id));
      if (data) {
        shoe.value = testdb.data;
        recommendations.value = testdb.data.filter((p) => p.id !== parseInt(id));
        isFetching.value = false;
      }
    } catch (err) {
      console.log(err);
    }
  };

  return {
    shoe,
    load,
    recommendations,
    isFetching,
  };
};

export default get;
