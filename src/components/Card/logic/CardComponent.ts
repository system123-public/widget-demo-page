import Price from '@/components/Price/PriceComponent.vue';
import { IShoe } from '@/interface/interface';
import { defineComponent, PropType } from 'vue';
import { useStore } from 'vuex';

export default defineComponent({
  name: 'CardComponent',
  props: {
    shoe: {
      type: Object as PropType<IShoe>,
      required: true,
    },
  },
  methods: {
    getImage: (pic: string) => {
      return require('@/assets/images/' + pic);
    },
  },
  components: {
    Price,
  },
  computed: {
    appIndicatorValue() {
      if (this.shoe.betterInApp === true) {
        return 'blitz';
      } else if (this.shoe.betterInApp === false) {
        return 'quicklook';
      } else {
        return '';
      }
    },
  },
  setup() {
    const store = useStore();
  },
});
