import Imgs from '@/helpers/imgs';
import { ref } from '@vue/reactivity';
import { defineComponent, onMounted, onUpdated } from 'vue';
import { testdb, testdbLoadingStatus } from '../../../testdb-promise';

export default defineComponent({
  name: 'ShowProduct',
  props: ['shoe', 'showButtons', 'changeShowModal'],

  beforeCreate() {
    const appleMeta = document.createElement('META');
    appleMeta.setAttribute('name', 'apple-itunes-app');
    appleMeta.setAttribute('content', 'app-id=1577511934, app-clip-bundle-id=ru.varstudio.vartube.Clip');

    document.querySelector('head')?.appendChild(appleMeta);

    window.addEventListener('message', (event) => {
      console.log(event);

      // if (event.origin !== 'https://widget.arigami.tech') return;

      let mediaStream: any = null;

      if (event.data === 'requestCamera') {
        navigator.mediaDevices
          .getUserMedia({
            video: true,
          })
          .then((stream) => {
            console.log(stream);
            mediaStream = stream;
          })
          .catch((err) => {
            console.error(err);
          });
      }

      if (event.data === 'stopUsingCamera') {
        if (mediaStream instanceof MediaStream) {
          mediaStream.getTracks().forEach((track) => track.stop());
        }
      }

      if (event.data === 'removeAppClipMeta') {
        appleMeta.remove();
      }
    });
  },

  setup() {
    const { originalImgs, imgs } = Imgs();
    const count = ref<number>(0);
    const showArWidget = ref<boolean>(false);
    const adaptiveState = ref<string>('large');
    const isWidgetFullscreen = ref<boolean>(false);

    const getImage = (pic: string) => {
      if (pic.includes('http')) return pic;
      else return require('@/assets/images/' + pic);
    };

    const next = () => {
      const MAXINDEX = 3;
      if (count.value < MAXINDEX) {
        count.value++;
      }
    };

    const prev = () => {
      const MININDEX = 0;
      if (count.value > MININDEX) {
        count.value--;
      }
    };

    const changeCount = (index: number) => {
      count.value = index;
    };

    const toggleArWidget = (value: boolean) => {
      showArWidget.value = value;
      isWidgetFullscreen.value = false;
    };

    const toggleWidgetFullscreen = () => {
      isWidgetFullscreen.value = !isWidgetFullscreen.value;

      if (isWidgetFullscreen.value) {
        document.body.style.overflow = 'hidden';
      } else {
        document.body.style.overflow = 'auto';
      }
    };

    const iframeElement = ref<HTMLElement>();
    const imageContainerElement = ref<HTMLDivElement>();

    const applyAdaptiveness = () => {
      const width = iframeElement.value?.clientWidth || imageContainerElement.value?.clientWidth || 0;
      const height = iframeElement.value?.clientHeight || imageContainerElement.value?.clientHeight || 0;
      const orientation = getOrientation(width, height);
      console.log(iframeElement.value?.clientWidth, imageContainerElement.value?.clientWidth);
      console.log(iframeElement.value?.clientHeight, imageContainerElement.value?.clientHeight);

      // console.log(width, height, orientation);

      if (width === undefined || height === undefined || !orientation) return;

      if ((width <= 400 && orientation.includes('portrait')) || (height <= 400 && orientation.includes('landscape'))) {
        adaptiveState.value = 'small';
      } else if (
        (width <= 600 && orientation.includes('portrait')) ||
        (height <= 600 && orientation.includes('landscape'))
      ) {
        adaptiveState.value = 'medium';
      } else if (
        (width >= 600 && orientation.includes('portrait')) ||
        (height >= 600 && orientation.includes('landscape'))
      ) {
        adaptiveState.value = 'large';
      }
    };

    const getOrientation = (width: number, height: number) => {
      // console.log(height, width, height >= width);

      if (height >= width) {
        // console.log('portrait');

        return 'portrait';
      } else {
        // console.log('landscape');

        return 'landscape';
      }
    };

    // applyAdaptiveness();
    window.addEventListener('orientationchange', () => {
      applyAdaptiveness();
    });
    window.addEventListener('resize', () => {
      applyAdaptiveness();
    });
    onMounted(() => {
      applyAdaptiveness();
    });
    onUpdated(() => {
      applyAdaptiveness();
    });

    const widgetUrl =
      window.location.href.includes('widget-dev') || window.location.href.includes('localhost')
        ? 'https://vroom.lab.arigami.tech'
        : 'https://vroom.arigami.tech';

    return {
      iframeElement,
      imageContainerElement,
      originalImgs,
      imgs,
      count,
      getImage,
      next,
      prev,
      changeCount,
      toggleArWidget,
      showArWidget,
      testdb,
      testdbLoadingStatus,
      adaptiveState,
      applyAdaptiveness,
      toggleWidgetFullscreen,
      isWidgetFullscreen,
      widgetUrl,
    };
  },
});
