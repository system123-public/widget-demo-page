import { computed, defineComponent, onMounted, PropType, ref } from 'vue';
import { useStore } from 'vuex';
import { featured } from '@/helpers/featured';
import router from '@/router';

export default defineComponent({
  name: 'ByTypes',
  
  props: {
    byBrand: {
      require: true,
      type: Boolean as PropType<boolean>,
    },
    byCategory: {
      require: true,
      type: Boolean as PropType<boolean>,
    },
  },

  setup() {
    const store = useStore();
    const filtersCategory = computed(() => store.state.filters['category']);
    const filtersBrand = computed(() => store.state.filters['brand']);
    const selectCategory = ref<string>('');
    const selectBrand = ref<string>('');

    onMounted(() => {
      if (filtersCategory.value !== '') selectCategory.value = filtersCategory.value;

      if (filtersBrand.value !== '') selectBrand.value = filtersBrand.value;
    });

    const { TYPES, BRANDS } = featured();
    
    const getCategory = (category_id: string) => {
      selectCategory.value = selectCategory.value === category_id ? '' : category_id;
      store.dispatch('set_filters', {
        filterType: 'category',
        filterValue: selectCategory,
      });
      store.state.showCatalog = false;
      if( router.currentRoute.value.path != '/' )
        router.push( {'path':'/'} );
    };

    const getBrand = (brand: string) => {
      selectBrand.value = selectBrand.value === brand ? '' : brand;
      store.dispatch('set_filters', {
        filterType: 'brand',
        filterValue: selectBrand,
      });
    };

    return {
      getBrand,
      getCategory,
      TYPES,
      BRANDS,
      selectCategory,
      selectBrand,
    };
  },
});
