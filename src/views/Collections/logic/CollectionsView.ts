import Card from '@/components/Card/CardComponent.vue';
import Pagination from '@/components/Pagination/PaginationComponent.vue';
import CardSkeleton from '@/components/Skeleton/Card/CardSkeleton.vue';
import { featured } from '@/helpers/featured';
import { computed, defineComponent, onMounted } from 'vue';
import { useStore } from 'vuex';

export default defineComponent({
  name: 'CollectionsView',
  components: {
    Card,
    Pagination,
    CardSkeleton,
  },
  computed: {
    currentCatalogName: function () {
      const store = useStore();
      const { TYPES } = featured();
      return TYPES.find((t) => t.id === store.state.filters['category']).text;
    },
  },

  setup() {
    const store = useStore();

    onMounted(() => {
      store.dispatch('getShoesData', {
        page: 1,
      });
    });

    const shoes = computed(() => store.getters.sortedSneakers);
    const isFetching = computed<boolean>(() => store.state.isFetching);
    const isEmpty = computed(() => store.getters.checkIsEmpty);
    const closeShowFilter = () => store.commit('changeShowFilter');

    return {
      shoes,
      isFetching,
      closeShowFilter,
      isEmpty,
      store,
    };
  },
});
