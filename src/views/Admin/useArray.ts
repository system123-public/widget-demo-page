import { EffectScope, effectScope, watch } from 'vue';

const scopes = new WeakMap<object, EffectScope>();

export function useArray<Item extends object>(array: Item[], useItem: (item: Item) => void) {
  watch(
    () => array,
    (newItems, oldItems) => {
      const addedComponents = new Set<Item>();
      const deletedComponents = new Set<Item>();

      if (oldItems) {
        for (const newComponent of newItems) {
          if (!oldItems.some((oldComponent) => oldComponent == newComponent)) {
            addedComponents.add(newComponent);
          }
        }

        for (const oldComponent of oldItems) {
          if (!newItems.some((newComponent) => newComponent == oldComponent)) {
            deletedComponents.add(oldComponent);
          }
        }
      } else {
        for (const newComponent of newItems) {
          addedComponents.add(newComponent);
        }
      }

      for (const deletedComponent of deletedComponents) {
        scopes.get(deletedComponent)!.stop();
      }

      for (const addedComponent of addedComponents) {
        const scope = effectScope();
        scopes.set(addedComponent, scope);

        scope.run(() => {
          useItem(addedComponent);
        });
      }
    },
    { immediate: true }
  );
}
